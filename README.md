# Indice
* [Rol Guest](#rol-guest-inicioindice)
* [Rol Reporter](#rol-reporter-inicioindice)
* [Rol Developer](#rol-developer-inicioindice)
* [Rol Maintainer](#rol-maintainer-inicioindice)

# Prueba Roles

Este proyecto sirve para poder probvar los roles y que ventajas se pueden hacer con cada uno

## Rol Guest ([Inicio](#indice))
Dentro de este rol vamos a poder hacer lo siguiente: 
* Se puede crear un Issue, pero solo se puede escribir, no se puede asignar miembros, tampoco time traking
* No se puede crear un Milestone
* No se puede crear Labels
* Desde el GitLab no se puede crear ramas
* En el GitKraken si se puede crear una rama local, crear archivos locales
* Puedo añadir el cambio realizado con commit
* No puedo hacer pull porque dice que la rama creada no esta en el origen
* No se puede hacer el Push porque el permiso no esta dado
* Se puede realizar el Merge pero no se puede hacer despues el push 

## Rol Reporter ([Inicio](#indice))
Dentro de este rol vamos a poder hacer lo siguiente: 
* Se puede crear un Issue 
* Se puede editar todas las configuraciones del Issue
* No se puede crear un Milestone
* Se puede crear Labels
* Desde el GitLab no se puede crear ramas
* En el GitKraken si se puede crear una rama local, crear archivos locales
* Puedo añadir el cambio realizado con commit
* No puedo hacer pull porque dice que la rama creada no esta en el origen
* No se puede hacer el Push porque el permiso no esta dado
* Se puede realizar el Merge pero no se puede hacer despues el push

## Rol Developer ([Inicio](#indice))
Dentro de este rol vamos a poder hacer lo siguiente: 
* Se puede crear un Issue 
* Se puede editar todas las configuraciones del Issue
* Se puede crear un Milestone
* Se puede crear Labels
* Desde el GitLab se puede crear ramas
* En el GitKraken si se puede crear una rama local, crear archivos locales
* Puedo añadir el cambio realizado con commit
* Puedo hacer pull para que se cree la rama en el origen
* Puedo hacer Merge enn ramas secundarias pero no en la rama master

## Rol Maintainer ([Inicio](#indice))
Dentro de este rol vamos a poder hacer lo siguiente: 
* Se puede crear un Issue 
* Se puede editar todas las configuraciones del Issue
* Se puede crear un Milestone
* Se puede crear Labels
* Desde el GitLab se puede crear ramas
* En el GitKraken si se puede crear una rama local, crear archivos locales
* Puedo añadir el cambio realizado con commit
* Puedo hacer pull para que se cree la rama en el origen
* Puedo hacer Merge en ramas secundarias 
* Puedo hacer Merge en la rama master
 


