var numero = 2.2;
var nombre1 = "Daniel";
var nombre2 = "Sebastián";
var nombreCompleto = nombre1 + nombre2;
var nombreCompleto2Forma = "Este es el nombre completo: "+ "Daniel" + "Sebastian";
var nombreCompleto3Forma = `Este es el nombre completo en la 3era forma ${nombre1} ${nombre2}`;

//console.log('nombre completo primera forma',nombreCompleto);
//console.log('nombre completo segunda forma',nombreCompleto2Forma);
//console.log('nombre completo tercera forma',nombreCompleto3Forma);

var banderas = true;
//console.log('bandera',banderas);
//banderas = 5;
//console.log('banderas 2', banderas);

var arreglo = ['fsadf', 'fasdf', 'fasdf', 4 , [] , true];
var objeto = {};

var apellido = null;
var direccion = undefined;

//console.log(typeof apellido);
//console.log(typeof direccion);
//console.log(typeof numero);
//console.log(typeof nombre1);
//console.log(typeof nombreCompleto3Forma);
//console.log(typeof banderas);
//console.log(typeof arreglo);
//console.log(typeof objeto);

if (apellido){
    //console.log('if',apellido);
}else {
    //console.log('else');
}

var numero1 = 3.5;
var nombre = "Daniel";
var booleano = true;
var arreglo = [1,2,3,4];
var nulo = null;
var noDefinido = undefined;

// if (numero1 == nombre) {
//     console.log("Valio");
// }else {
//     console.log("No valio");
// }
// if (numero1 === booleano) {
//     console.log("Valio");
// }else {
//     console.log("No valio");
// }
// if (numero1 === arreglo) {
//     console.log("Valio");
// }else {
//     console.log("No valio");
// }
// if (numero1 === nulo) {
//     console.log("Valio");
// }else {
//     console.log("No valio");
// }
// if (numero1 === noDefinido) {
//     console.log("Valio");
// }else {
//     console.log("No valio");
// }
// if (nombre === booleano) {
//     console.log("Valio");
// }else {
//     console.log("No valio");
// }
// if (nombre == arreglo) {
//     console.log("Valio");
// }else {
//     console.log("No valio");
// }

switch (apellido) {
    case "a":
        break;
    case "b":
        break;
}

function sumar(a , b) {
    return a + b;
}
var resultado = sumar(4,5);
//console.log(resultado);

var suma = function (a,b) {
    return a+b;
};
/*console.log(suma(5,1));
console.log(suma([1,2,3],[4,5,6]));
console.log(suma([1,2,3],"Daniel"));
console.log(suma("Tu","Daniel"));
console.log(suma(null,"-Daniel"));
console.log(suma(null,null));
console.log(suma(null,undefined));
console.log(suma(null,false));
console.log(suma(null,true));
console.log(suma(false,true));
console.log(suma(true,true));
console.log(suma(undefined,true));
console.log(suma(undefined,undefined));
console.log(suma([1,2,3],undefined));
console.log(suma([1,2,3],undefined));*/

var valorDefault = function (a,b=4) {
    return a+b;
};
//console.log(valorDefault(4));

function ejecutarFunciones(funcion) {
    return funcion('Daniel')
}

function saludar(nombre) {
    return 'hola' + nombre;
}
function despedirse(nombre) {
    return 'adios' + nombre;
}

/*console.log(ejecutarFunciones(saludar));
console.log(ejecutarFunciones(despedirse));*/

var suma = function (a,b) {
    return a + b;
};
var suma = (a,b) => a+b;

const calculadora = (funcion) => funcion(1,2);
const sumado = (a,b) => a+b;
const restar = (a,b) => a-b;
const multiplicar = (a,b) => a*b;
const dividir = (a,b) => a/b;

/*console.log(calculadora(sumado));
console.log(calculadora(restar));
console.log(calculadora(multiplicar));
console.log(calculadora(dividir));*/

const persona = {
    nombre : "Daniel",
    apellido : "Ramírez",
    direcciones: [
        {
            callePrincipal: "fjdsk",
            calleSecundaria: "dasani"
        },
        {
            callePrincipal: "daniel",
            calleSecundaria: "sebastian"
        }
    ]
};
/*console.log('persona: ', persona);
console.log('nombre: ',persona.nombre);
console.log('apellido: ',persona.apellido);
console.log('direccion: ',persona.direccion);
persona.apellido = 'Buendía';
console.log('Nuevo apellido: ',persona.apellido);*/

const funcionesCalculadora = {
    sum : (a,b) => a+b,
    res : (a,b) => a-b,
    mul : (a,b) => a*b,
    div : (a,b) => a/b,
    potenciacion: undefined,
    radicacion: undefined
};
const calcu = (fun, a, b) => fun(a,b);
/*console.log(calcu(funcionesCalculadora.sum,4,4));
console.log(calcu(funcionesCalculadora.res,7,2));
console.log(calcu(funcionesCalculadora.mul,83,532));
console.log(calcu(funcionesCalculadora.div,24,65));
console.log(funcionesCalculadora);
delete funcionesCalculadora.potenciacion;
delete funcionesCalculadora.radicacion;
funcionesCalculadora.nuevoAtributo = 4;
console.log(funcionesCalculadora);*/

const operacionesCientifica = Object.assign({},funcionesCalculadora);
let operacionesAvanzadas = JSON.stringify(funcionesCalculadora);
operacionesAvanzadas = JSON.parse(operacionesAvanzadas);
/*console.log(operacionesAvanzadas);*/

console.log(Object.keys(funcionesCalculadora));




