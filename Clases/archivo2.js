/*while(condicion){
    //ejecuta código mientras cumple la condicion
}

do {
    //ejecuta el código
}while (condicion);*/

let numero = 1;
//console.log('Impresion ascendente');
while (numero<=20){
    //console.log('El numero es: ',numero);
    numero+=1;
}
numero-=1;
//console.log('Impresion descentente');
while (numero>=1){
    //console.log('El numero es: ',numero);
    numero-=1;
}
numero+=1;
//console.log('Impresion ascendente');
do {
    //console.log('El numero es: ', numero);
    numero+=1;
}while (numero<=20);

const nombre = "Daniel";
const nombreCompleto = "Daniel Sebastián Ramírez Buendía";

//Longitud
/*console.log('longitud del nombre: ', nombre.length);
console.log('longitud del nombre completo: ', nombreCompleto.length);
console.log('indice de la letra a: ', nombreCompleto.indexOf('i'));
console.log('indice de la expresion regular: ', nombreCompleto.search('/Ramírez'));
console.log('indice de la expresion regular: ', nombreCompleto.charAt(14));*/

const posicionLetra = nombreCompleto.indexOf('á');
//console.log('Letra: ',nombreCompleto.charAt(posicionLetra));

const nombreConEspacios = " Tania Buendia ";
//console.log("Longitud inicial:",nombreConEspacios.length);
const nombreSinEspacios = nombreConEspacios.trim();
//console.log("Longitud sin espacios:",nombreSinEspacios.length);

const indiceInicio = nombreCompleto.search(/Sebastián/);
const indiceFinal = nombreCompleto.search(/Ramírez/);
const palabraBuscarda = nombreCompleto.slice(indiceInicio,indiceFinal);
//console.log('Palabra con slice:',palabraBuscarda);

const arregloNombreSeparado = nombreCompleto.split('');
//console.log("Nombre Completo separado en array:",arregloNombreSeparado);

const ocurrenciaLetraLastIndexOf = nombreCompleto.lastIndexOf('m');
const ocurrenciaLetraIndexOf = nombreCompleto.indexOf('m');
//console.log('Indice con lastIndexOf:',ocurrenciaLetraLastIndexOf);
//console.log('Indice con indexOf:',ocurrenciaLetraIndexOf);

//console.log(nombreCompleto);

//Arreglos

const arregloNumeros = [1,2,3,4,5,6,7,8,9,10];
const arregloLetras = ['a','b','c','d,','e','f'];
const arregloLetrasNumeros = [];
const tamnioArregloNumeros = arregloNumeros.length;
//console.log("Tamaño del arreglo de numeros es:",tamnioArregloNumeros);

//console.log('Arreglo antes:',arregloNumeros);
arregloNumeros.push(11);
//console.log('Arreglo despues:',arregloNumeros);

arregloNumeros.unshift(0);
//console.log('Arreglo despues del despues:',arregloNumeros);

arregloNumeros[20] = 7;
const indiceArregloBuscado = arregloNumeros.indexOf(6);
//console.log('Indice del arreglo buscado:',indiceArregloBuscado);

const arrayEliminadoUltimoElemento = arregloNumeros.pop();
//console.log('Elemento eliminado:',arrayEliminadoUltimoElemento);

/*const conSplice = arregloNumeros.splice(1,3);
console.log('original:',arregloNumeros);
console.log('con splice:',conSplice);*/

const conSlice = arregloNumeros.slice(1,3);
//console.log('original:',arregloNumeros);
//console.log('con splice:',conSlice);

const eliminarNumero = (arregloNumeros,numeroAEliminar) => {
    const obtenerIndexNumero = arregloNumeros.indexOf(numeroAEliminar);
    if(obtenerIndexNumero!==-1){
        arregloNumeros.splice(obtenerIndexNumero,1);
        return arregloNumeros;
    }else{
        return arregloNumeros;
    }

};

//console.log('Array original:',arregloNumeros);
const arregloFinal = eliminarNumero(arregloNumeros,45);
//console.log('Array fibnal:',arregloFinal);

arregloNumeros.forEach((valor,indice,mismoArreglo)=>{
    //console.log('Valor',valor);
    //console.log('Indices',indice);
    //console.log('Mismo arreglo',mismoArreglo);
});
const every = arregloNumeros.every((valor)=>{
    return valor >= 0;
});
//console.log(every);
const every2 = arregloNumeros.every((valor)=>{
    return typeof valor === 'number';
});
//console.log(every2);

const arregloElementosFiltrados = arregloNumeros.filter((valor)=>{
   return  valor > 3 && valor < 9
});
//console.log(arregloElementosFiltrados);

const valorFind = arregloNumeros.find((valor)=>{
   return valor === 5;
});
//console.log('valor find:', valorFind);

const booleanSome = arregloNumeros.some((valor)=>{
   return valor === 10;
});
//console.log('El resultado es:',booleanSome);

const arregloMap = arregloNumeros.map((valor)=>{
   return valor === 10;
});
//console.log(arregloMap);

var restarElementos = arregloNumeros.reduce((acumulador, valor)=>{
    //console.log(acumulador);
    return acumulador - valor;
},10);
//console.log(restarElementos);

const personas = [
    {
        nombre: 'Andrea',
        apellido: 'Silva',
        ciudad: 'Quito'
    },
    {
        nombre: 'Gabriel',
        apellido: 'Buendía',
        ciudad: 'Barcelona'
    },
    {
        nombre: 'Daniel',
        apellido: 'Ramírez',
        ciudad: 'Quito'
    },
    {
        nombre: 'Liseth',
        apellido: 'Castillo',
        ciudad: 'Guayaquil'
    }
];

const personasReduce = personas.reduce((acumulador,valor)=>{
    console.log(acumulador);
    const esDeQuito = valor.ciudad === 'Quito';
    if(esDeQuito){
        acumulador.push(valor);
    }
    return acumulador;
},[])
    .map((valor)=>{
        valor.ciudad = "QUITO";
    })
    .every((valor)=>{
        return typeof valor.nombre === 'string';
    });

console.log('Es de quito:',personasReduce);

//listar los atributos que tiene un usuario
//dos arreglos uno en los que estan activos y otros en los que estan desactivos
//cuantas personas son mayores de 20, 30 y 40 años y colocarlos en array
//poner un nuevo arreglo de las personas con el nombre, apellido y el color de los ojos
//el atributo about solo sea una oracion de 10 palabras y antes de modificar cuantos caracteres tenia
//cuantos amigos tiene cada uno
//a cuantos les gusta la manzana, la banana, y el strawberry
//cuantos usuarios son originalmente
//



